# modify to set number of pages
# TODO take command line arg for this
pages = 20
outTex = "blankPages.tex"

with open(outTex, 'w') as f:
    f.write("\\documentclass[a4paper]{article}\n")
    f.write("\\begin{document}\n")

    for p in range(0, pages):
        f.write("\\newpage \\thispagestyle{empty} \\mbox{} \n")

    f.write("\\end{document}\n")
